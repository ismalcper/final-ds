<%@ page import="java.util.*" %>
<%@ page import="mx.uaemex.fi.diseno.nrda.model.data.Pregunta" %>
<!DOCTYPE HTML>
<!--
	Phase Shift by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
<title>login - Phase Shift by TEMPLATED</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.dropotron.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/skel-layers.min.js"></script>
<script src="js/init.js"></script>

<link rel="stylesheet" href="css/skel.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/style-wide.css" />
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
</head>
<body>

	<!-- Wrapper -->
	<div class="wrapper style1">

		<!-- Header -->
		<div id="header" class="skel-panels-fixed">
			<div id="logo">
				<h1>
					<a href="index.html"></a>Flyn's Arcade
				</h1>
				<span class="tag">Basado en Phase Shift by TEMPLATED</span>
			</div>

			<%@ include file="../menu1.jsp"%>
			<!--Salir-->
			<!--<nav id="nav">
								<ul>
								<li class="active"><a href="index.html">Homepage</a></li>
								<li><a href="left-sidebar.html">Left Sidebar</a></li>
								<li><a href="right-sidebar.html">Right Sidebar</a></li>
								<li><a href="no-sidebar.html">No Sidebar</a></li>
							</ul>
						</nav> -->
		</div>
		<!-- Header -->


		<!-- Main -->
		<div id="main" class="displayed">
			<div class="container">
				<center>
					<img class="displayed" src="images/flyns.png" alt="Flyn's Arcade" />
				</center>
			</div>
		</div>
		<!-- /Main -->

	</div>

	<!-- Footer -->
	<div id="footer" class="wrapper style2">
		<div class="container">
			<section>
				<header class="major">
					<h2>Registro de Usuario</h2>
				</header>
				<form method="post" action="registrar">
					<h4>Datos Personales</h4>
					<hr>
					<div class="row half">
						<div class="12u">
							<input class="text" type="text" name="nombre" id="nombre"
								placeholder="Nombre" />
						</div>
					</div>
					<div class="row half">
						<div class="12u">
							<input class="text" type="text" name="primer_apellido"
								id="primer_apellido" placeholder="Primer apellido" />
						</div>
					</div>
					<div class="row half">
						<div class="12u">
							<input class="text" type="text" name="segundo_apellido"
								id="segundo_apellido" placeholder="Segundo apellido" />
						</div>
					</div>
					<div class="row half">
						<div class="12u">
							<input class="text" type="number" min="0" max="200" name="edad"
								id="edad" placeholder="Edad" />
						</div>
					</div>

					<br>
					<h4 style="text-aling: left;">Datos de la cuenta</h4>
					<hr>
					<div class="row half">
						<div class="12u">
							<input class="text" type="text" name="login" id="login"
								placeholder="Nombre de usuario" />
						</div>
					</div>
					<div class="row half">
						<div class="12u">
							<input class="text" type="password" name="password" id="password"
								placeholder="Contraseņa" />
						</div>
					</div>
					<div class="row half">
						<div class="12u">
							<input class="text" type="password" name="password2"
								id="password2" placeholder="Repetir contraseņa" />
						</div>
					</div>

					<div class="row half">
						<div class="12u">
							<select class="text" name="idPregunta" id="pregunta">
								<option value="0" selected="true" disabled>Selecciona
									una pregunta de seguridad</option>
								
								<%
								ArrayList<Pregunta> preguntas = new ArrayList<>();
								preguntas = (ArrayList<Pregunta>)request.getAttribute("preguntas");
								
								Iterator<Pregunta> it = preguntas.iterator();
								while(it.hasNext()){
									Pregunta item=it.next(); %>
									<option value="<%=item.getId()%>"><%=item.getPregunta()%></option>
								<%}
								
							%>

							</select>						
						</div>
					</div>
					<div class="row half">
						<div class="12u">
							<input class="text" type="text" name="respuesta" id="respuesta"
								placeholder="Repuesta" />
						</div>
					</div>




					<div class="row half">
						<div class="12u">
							<ul class="actions">
								<li><input type="submit" value="Registrar"
									class="button alt" /></li>
							</ul>
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>
	<!-- /Footer -->

	<!-- Copyright -->
	<div id="copyright">
		<div class="container">
			<span class="copyright">Design: <a href="http://templated.co">TEMPLATED</a>
				Images: <a href="http://unsplash.com">Unsplash</a> (<a
				href="http://unsplash.com/cc0">CC0</a>)
			</span>
			<ul class="icons">
				<li><a href="#" class="fa fa-facebook"><span>Facebook</span></a></li>
				<li><a href="#" class="fa fa-twitter"><span>Twitter</span></a></li>
				<li><a href="#" class="fa fa-google-plus"><span>Google+</span></a></li>
			</ul>
		</div>
	</div>

</body>
</html>