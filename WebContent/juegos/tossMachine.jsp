<%@ page import="java.util.Date" %> 
<%@ include file="../valida_sesion.jsp" %>
<!DOCTYPE HTML>
<!--
	Phase Shift by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>

<head>
  <title>Toss Machine</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
  <script src="../js/jquery.min.js"></script>
  <script src="../js/jquery.dropotron.min.js"></script>
  <script src="../js/skel.min.js"></script>
  <script src="../js/skel-layers.min.js"></script>
  <script src="../js/init.js"></script>

  <link rel="stylesheet" href="../css/skel.css" />
  <link rel="stylesheet" href="../css/style.css" />
  <link rel="stylesheet" href="../css/style-wide.css" />
  <link href="../css/style2.css" rel="stylesheet">
  <noscript>
    <link rel="stylesheet" href="css/skel.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/style-wide.css" />
  </noscript>
  <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
</head>

<body>

  <!-- Wrapper -->
  <div class="wrapper style1">

    <!-- Header -->
    <div id="header" class="skel-panels-fixed">
      <div id="logo">
        <h1><a href="index.html">Flyn's Arcade</a></h1>
        <span class="tag"></span>
      </div>
      <%@ include file="../menu3.jsp"%>
    </div>
    <!-- Header -->

    <!-- Page -->
    <div id="page" class="container">
      <section>
        <header class="major">
          <h2>Toss Machine</h2>
          <span class="byline"> "No puedes deshacer lo que ya has hecho, pero s� puedes enfrentarlo." </span>
        </header>
      </section>
    </div>
    <!-- /Page -->
    <!-- Main -->
    <div id="main">
      <hr />
      <div class="container" style="background-color:#FFF;" align="center">
        <div class="row">
          <div class="row half">
            <br>
            <h2 style="color: #000"> &nbsp;&nbsp;&nbsp;&nbsp;�Cara o cruz? Tu vida depende de ello...</h2>
            <div class="12u" style="color: #000">
              <br>
            </div>
            <div class="6u" style="color: #000">
              <img src="../images/tossMachine/tenor22222.gif" width="444" height="296" / alt="3">
            </div>
            <div class="6u" style="color: #000">
              <br><br>
              <form method="post" action="#">
                <div class="row half">
                  <div class="12u">
                     <select id="eleccionMoneda">
					   <option value="1">Sol</option>
					   <option value="3">�guila</option>
				     </select>
				     <div id="coin" onclick="miclick()">
				        <div class="front"></div>
				        <div class="back"></div>
				     </div>   
                  </div>
                </div>
                <div class="row half">
                  <div class="12u">
                    <p id="result"></p>                  
                    <ul class="actions">
                      <li>
                      </li>
                    </ul>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /Main -->

  </div>

  <!-- Footer -->
  <div id="footer" class="wrapper style2">
    <div class="container">
      <section>
        <header class="major">
          <h2>Arcade</h2>
          <span class="byline">Tributo a Tron y a los 80's</span>
          <img class="displayed" src="../images/flyns.png" alt="Flyn's Arcade" />
        </header>
        <form method="post" action="principal.html">
          <div class="row half">
            <div class="12u">
              <ul class="actions">
                <li>
                  <input type="submit" value="Salir del juego" class="button alt" />
                </li>
              </ul>
            </div>
          </div>
        </form>
      </section>
    </div>
  </div>
  <!-- /Footer -->
  <!-- Copyright -->
  <div id="copyright">
    <div class="container"> <span class="copyright">Design: <a href="http://templated.co">TEMPLATED</a> Images: <a
          href="http://unsplash.com">Unsplash</a> (<a href="http://unsplash.com/cc0">CC0</a>)</span>
      <ul class="icons">
        <li><a href="#" class="fa fa-facebook"><span>Facebook</span></a></li>
        <li><a href="#" class="fa fa-twitter"><span>Twitter</span></a></li>
        <li><a href="#" class="fa fa-google-plus"><span>Google+</span></a></li>
      </ul>
    </div>
  </div>
    <script type="text/javascript" src='../js/jquery-migrate.min.js'></script>
    <script type="text/javascript" src='../js/coin.js'></script>
    <script>
    setTimeout(miclick,4000);
    function miclick(){
    	var elemento = document.getElementById('coin').className;
    	console.log(elemento+" hola ");
    	console.log(document.getElementById("eleccionMoneda").value +" hola ");
    }
    
    
    
    $("#coin").click(function() {
    	var elemento = document.getElementById('coin').className;
    	console.log(elemento);
    	console.log(document.getElementById("eleccionMoneda").value);
    	if(elemento=="animation900"){//aguila 
     		if(document.getElementById("eleccionMoneda").value=='1'){//sol
       	        document.getElementById('result').innerText="Perdiste, el d�a de tu muerte ha llegado";	
    		}else{
       	        document.getElementById('result').innerText="Ganaste, por hoy seguiras viviendo";
    		}
    	}
        if(elemento=="animation1080"){//sol
        	if(document.getElementById("eleccionMoneda").value=='1'){//sol
       	        document.getElementById('result').innerText="Ganaste, por hoy seguiras viviendo";
    		}else{
       	        document.getElementById('result').innerText="Perdiste, el d�a de tu muerte ha llegado";	
    		}
    	}
        if(elemento=="animation1260"){//aguila
     		if(document.getElementById("eleccionMoneda").value=='1'){//sol
       	        document.getElementById('result').innerText="Perdiste, el d�a de tu muerte ha llegado";	
    		}else{
       	        document.getElementById('result').innerText="Ganaste, por hoy seguiras viviendo";
    		}
    	}
        if(elemento=="animation1440"){//sol
        	if(document.getElementById("eleccionMoneda").value=='1'){//sol
       	        document.getElementById('result').innerText="Ganaste, por hoy seguiras viviendo";
    		}else{
       	        document.getElementById('result').innerText="Perdiste, el d�a de tu muerte ha llegado";	
    		}
    	}
        if(elemento=="animation1620"){//aguila 
     		if(document.getElementById("eleccionMoneda").value=='1'){//sol
       	        document.getElementById('result').innerText="Perdiste, el d�a de tu muerte ha llegado";	
    		}else{
       	        document.getElementById('result').innerText="Ganaste, por hoy seguiras viviendo";
    		}
    	}
        if(elemento=="animation1800"){//sol
        	if(document.getElementById("eleccionMoneda").value=='1'){//sol
       	        document.getElementById('result').innerText="Ganaste, por hoy seguiras viviendo";
    		}else{
       	        document.getElementById('result').innerText="Perdiste, el d�a de tu muerte ha llegado";	
    		}
        }
        if(elemento=="animation1980"){//aguila
     		if(document.getElementById("eleccionMoneda").value=='1'){//sol
       	        document.getElementById('result').innerText="Perdiste, el d�a de tu muerte ha llegado";	
    		}else{
       	        document.getElementById('result').innerText="Ganaste, por hoy seguiras viviendo";
    		}
    	}
        if(elemento=="animation2160"){//sol
        	if(document.getElementById("eleccionMoneda").value=='1'){//sol
       	        document.getElementById('result').innerText="Ganaste, por hoy seguiras viviendo";
    		}else{
       	        document.getElementById('result').innerText="Perdiste, el d�a de tu muerte ha llegado";	
    		}
    	}
    });
    </script>
    
</body>

</html>