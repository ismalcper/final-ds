<%@ page import="java.util.Date" %> 
<%@ include file="../valida_sesion.jsp" %>
<!DOCTYPE HTML>
<!--
	Phase Shift by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>

<head>
  <title>x Botellas de cerveza</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
  <script src="../js/jquery.min.js"></script>
  <script src="../js/jquery.dropotron.min.js"></script>
  <script src="../js/skel.min.js"></script>
  <script src="../js/skel-layers.min.js"></script>
  <script src="../js/init.js"></script>

  <link rel="stylesheet" href="../css/skel.css" />
  <link rel="stylesheet" href="../css/style.css" />
  <link rel="stylesheet" href="../css/style-wide.css" />
  <noscript>
    <link rel="stylesheet" href="css/skel.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/style-wide.css" />
  </noscript>
  <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
</head>

<body>

  <!-- Wrapper -->
  <div class="wrapper style1">

    <!-- Header -->
    <div id="header" class="skel-panels-fixed">
      <div id="logo">
        <h1><a href="index.html">Flyn's Arcade</a></h1>
        <span class="tag"></span>
      </div>
      <%@ include file="../menu3.jsp"%>
    </div>
    <!-- Header -->

    <!-- Page -->
    <div id="page" class="container">
      <section>
        <header class="major">
          <h2>x Botellas de cerveza en la pared</h2>
          <span class="byline">Puedo cantar la canci�n de X botellas de cerveza en la pared,
        siempre y cuando X sea un n�mero entre 1 y 100.</span>
        </header>
      </section>
    </div>
    <!-- /Page -->
    <!-- Main -->
    <div id="main">
      <hr />
      <div class="container" style="background-color:#FFF;" align="center">
        <div class="row">
          <div class="row half">
            <br>
            <h2 style="color: #000">�Y tu cu�ntas puedes cantar?</h2>
            <div class="12u" style="color: #000">
              <br>
            </div>
            <div class="6u" style="color: #000">
              <img src="../images/xBotellas/botellas.gif" width="444" height="296" / alt="3">
            </div>
            <div class="6u" style="color: #000">
              <br><br>
              <form method="post" action="../controlBotellas">
                <div class="row half">
                  <div class="12u">
                    <input class="text" type="text" name="xBotellas" id="xBotellas"
                      placeholder="�Cu�ntas quieres que cante?" />
                  </div>
                </div>
                <div class="row half">
                  <div class="12u">
                    <ul class="actions">
                      <li>
                        <input type="submit" value="Cantar" class="button alt" />
                      </li>
                    </ul>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /Main -->

  </div>

  <!-- Footer -->
  <div id="footer" class="wrapper style2">
    <div class="container">
      <section>
        <header class="major">
          <h2>Arcade</h2>
          <span class="byline">Tributo a Tron y a los 80's</span>
          <img class="displayed" src="../images/flyns.png" alt="Flyn's Arcade" />
        </header>
        <form method="post" action="principal.html">
          <div class="row half">
            <div class="12u">
              <ul class="actions">
                <li>
                  <input type="submit" value="Salir del juego" class="button alt" />
                </li>
              </ul>
            </div>
          </div>
        </form>
      </section>
    </div>
  </div>
  <!-- /Footer -->
  <!-- Copyright -->
  <div id="copyright">
    <div class="container"> <span class="copyright">Design: <a href="http://templated.co">TEMPLATED</a> Images: <a
          href="http://unsplash.com">Unsplash</a> (<a href="http://unsplash.com/cc0">CC0</a>)</span>
      <ul class="icons">
        <li><a href="#" class="fa fa-facebook"><span>Facebook</span></a></li>
        <li><a href="#" class="fa fa-twitter"><span>Twitter</span></a></li>
        <li><a href="#" class="fa fa-google-plus"><span>Google+</span></a></li>
      </ul>
    </div>
  </div>

</body>

</html>