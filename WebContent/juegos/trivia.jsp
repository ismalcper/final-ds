<%@ page import="java.util.Date"%>
<%@ include file="../valida_sesion.jsp"%>
<!DOCTYPE HTML>
<!--
	Phase Shift by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>

<head>
<title>Potro Trivia</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dropotron.min.js"></script>
<script src="../js/skel.min.js"></script>
<script src="../js/skel-layers.min.js"></script>
<script src="../js/init.js"></script>

<link rel="stylesheet" href="../css/skel.css" />
<link rel="stylesheet" href="../css/style.css" />
<link rel="stylesheet" href="../css/style-wide.css" />
<noscript>
	<link rel="stylesheet" href="css/skel.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-wide.css" />
</noscript>
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
</head>

<body>

	<!-- Wrapper -->
	<div class="wrapper style1">

		<!-- Header -->
		<div id="header" class="skel-panels-fixed">
			<div id="logo">
				<h1>
					<a href="../principal.jsp">Flyn's Arcade</a>
				</h1>
				<span class="tag"></span>
			</div>
			<%@ include file="../menu3.jsp"%>
		</div>
		<!-- Header -->

		<!-- Page -->
		<div id="page" class="container">
			<section>
				<header class="major">
					<h2>Potro Trivia</h2>
					<span class="byline"> � Eres potro hermano ?</span>
				</header>
			</section>
		</div>
		<!-- /Page -->
		<!-- Main -->
		<div id="main">
			<hr />
			<div class="container" style="background-color: #FFF;" align="center">
				<div class="row">
					
						<div class="6u" style="color: #000">
							<img src="../images/trivia/tenor33.gif" width="444"
								height="296" / alt="3">
						</div>
						<div class="6u" style="color: #000">
							<br> <br>
							<form method="post" action="#">
								<div class="row half">
										<div class="6u">
										<div id="pregunta"></div>
										<div id="respuestas"></div>

										<input type="button" value="Responder" onclick="comprobar()"><br><br>
										<input type="button" value="Terminar"  onclick="terminar()">
									</div>
								</div>
								<div>
									<div class="6u">
									</div>
								</div>
								
									
								</div>

							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Main -->

	</div>

	<!-- Footer -->
	<div id="footer" class="wrapper style2">
		<div class="container">
			<section>
				<header class="major">
					<h2>Arcade</h2>
					<span class="byline">Tributo a Tron y a los 80's</span> <img
						class="displayed" src="../images/flyns.png" alt="Flyn's Arcade" />
				</header>
				<form method="post" action="../principal.jsp">
					
					<div class="row half">
						<div class="12u">
							<ul class="actions">
								<li><input type="submit" value="Salir del juego"
									class="button alt" /></li>
							</ul>
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>
	<!-- /Footer -->
	<!-- Copyright -->
	<div id="copyright">
		<div class="container">
			<span class="copyright">Design: <a href="http://templated.co">TEMPLATED</a>
				Images: <a href="http://unsplash.com">Unsplash</a> (<a
				href="http://unsplash.com/cc0">CC0</a>)
			</span>
			<ul class="icons">
				<li><a href="#" class="fa fa-facebook"><span>Facebook</span></a></li>
				<li><a href="#" class="fa fa-twitter"><span>Twitter</span></a></li>
				<li><a href="#" class="fa fa-google-plus"><span>Google+</span></a></li>
			</ul>
		</div>
	</div>

	<script>
	
		var preguntas =["Nombre del actual Rector ... ", 
			"Presidente que inaugura la Ciudad Universitaria de Toluca...",
			"A�o que se inaugura Ciudad Universitaria de la UAEMEX ...", 
			"�Cu�ntos centros universitarios tiene la UAEMEX?", 	
			"Colores acad�micos de la UAEMEX",
			"Mascota oficial de la UAEMEX",
			"Lema de la UAEMEX",
			"�Existe un centro universitario en el municipio de Ecatepec?",
			"Ingenier�a en pl�sticos, �es una carrera de la UAEMEX?",
			"Cantidad de licenciaturas e Ingenier�as en la UAEMEX"];
		
		var respuestas=[["Alfredo Barrera Vaca","Alfredo Vaca Barrera","Jorge Olvera"],
			["Adolfo L�pez Mateos","L�zaro C�rdenas","Venustiano Carranza"],
			["1992","1956","1882"],
			["23","10","11"],
			["Verde y Oro","Oro y Blanco","Verde y Blanco"],
			["Potro","Pony","Puma"],
			["Patria, Ciencia y Trabajo","Ninguno de los Anteriores","Trabajo, Ciencia y Humanismo"],
			["Verdadero","Falso", "Tal vez"],
			["Si","No","Tal vez"],
			["82","4","32"]];
		
		jugar();
		
		var indicieRespuestaCorrecta;
		var puntos=0;
		
		
		function jugar(){
			var numAleatorio= Math.floor(Math.random()*preguntas.length);

			var respuestasPosibles = respuestas[numAleatorio];
	
	
			var posiciones = [0,1,2];
			var respuestasReordenadas = [];
	
			var ya_se_metio = false;
			for(i=0;i<=5;i++){
				pregunta[i]
			}
			for(i in respuestasPosibles){
				var posicionAleatoria = Math.floor(Math.random()*posiciones.length);
				if(posicionAleatoria==0 && ya_se_metio == false){
					indicieRespuestaCorrecta =i;
					
					ya_se_metio = true;
				}
				respuestasReordenadas[i] = respuestasPosibles[posiciones[posicionAleatoria]];
				posiciones.splice(posicionAleatoria, 1);
			}
	
			var txtRespuestas="";
			for(i in respuestasReordenadas){
				txtRespuestas += '<input type="radio" name="pp" value="'+i+'"><label>'+respuestasReordenadas[i]+'</label><br><br>';
			}
	
			document.getElementById("respuestas").innerHTML = txtRespuestas;
			document.getElementById("pregunta").innerHTML = preguntas[numAleatorio];

		}
		
		function comprobar(){
			var respuesta = $("input[type=radio]:checked").val();

			if(respuesta ==indicieRespuestaCorrecta){
				alert("Muy bien!");
				puntos=puntos+1;
			}else{
				alert("�Seguro que eres potro hermano?");
			}
			jugar();
		}
		
		//AQUI SE MUESTRAN LOS PUNTOS ACUMULADOS
		
		function terminar(){
			alert("Felicidades! Tus puntos fueron:  "+puntos);
		}
		
		
	</script>

</body>


</html>