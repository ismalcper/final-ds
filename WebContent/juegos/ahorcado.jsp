<%@ page import="java.util.Date"%>
<%@ include file="../valida_sesion.jsp"%>
<!DOCTYPE HTML>
<!--
	Phase Shift by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>

<head>
<title>Ahorcado</title>
<meta http-equiv="content-type" content="text/html"; charset="utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.dropotron.min.js"></script>
<script src="../js/skel.min.js"></script>
<script src="../js/skel-layers.min.js"></script>
<script src="../js/init.js"></script>

<link rel="stylesheet" href="../css/skel.css" />
<link rel="stylesheet" href="../css/style.css" />
<link rel="stylesheet" href="../css/style-wide.css" />
<noscript>
	<link rel="stylesheet" href="css/skel.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-wide.css" />
</noscript>
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
</head>

<body>

	<!-- Wrapper -->
	<div class="wrapper style1">

		<!-- Header -->
		<div id="header" class="skel-panels-fixed">
			<div id="logo">
				<h1>
					<a href="../principal.jsp">Flyn's Arcade</a>
				</h1>
				<span class="tag"></span>
			</div>
			<%@ include file="../menu3.jsp"%>
		</div>
		<!-- Header -->

		<!-- Page -->
		<div id="page" class="container">
			<section>
				<header class="major">
					<h2>AHORCADO</h2>
					<span class="byline"> "Cuanto m�s te acerques a la luz, mayor ser� tu sombra" </span>
				</header>
			</section>
		</div>
		<!-- /Page -->
		<!-- Main -->
		<div id="main">
			<hr />
			<div class="container" style="background-color: #FFF;" align="center">
				<div class="row">
					<div class="row half">
						<br>
						<h2 style="color: #000">&nbsp;&nbsp;&nbsp;&nbsp;No mueras</h2>
						<div class="12u" style="color: #000">
							<br>
						</div>
						<div class="6u" style="color: #000">
							<img src="../images/ahorcado/tenor22.gif" width="444"
								height="296" / alt="3">
						</div>
						<div class="6u" style="color: #000">
							<br> <br>
							<form method="post" action="#">
								<div class="row half">
									<div class="6u"></div>
									<div class="6u">
										<button type="button" id="iniciar" name="iniciar">Iniciar</button>
										<a href="ahorcado.jsp">Volver a jugar</a>
									</div>
								</div>
								<div class="row half">
									<div class="4u">
										<input type="hidden" name="respuesta" id="respuesta" value="">
										<input type="hidden" name="letrasEncontradas"
											id="letrasEncontradas" value="">
										<h5>Elige una letra</h5>
										<table>
											<table class="letras">
												<tr>
													<td><button id="a" onclick="buscarLetra('a')"
															type="button">A</button></td>
													<td><button id="b" onclick="buscarLetra('b')"
															type="button">B</button></td>
													<td><button id="c" onclick="buscarLetra('c')"
															type="button">C</button></td>
												</tr>
												<tr>
													<td><button id="d" onclick="buscarLetra('d')"
															type="button">D</button></td>
													<td><button id="e" onclick="buscarLetra('e')"
															type="button">E</button></td>
													<td><button id="f" onclick="buscarLetra('f')"
															type="button">F</button></td>
												</tr>
												<tr>
													<td><button id="g" onclick="buscarLetra('g')"
															type="button">G</button></td>
													<td><button id="h" onclick="buscarLetra('h')"
															type="button">H</button></td>
													<td><button id="i" onclick="buscarLetra('i')"
															type="button">I</button></td>
												</tr>
												<tr>
													<td><button id="j" onclick="buscarLetra('j')"
															type="button">J</button></td>
													<td><button id="k" onclick="buscarLetra('k')"
															type="button">K</button></td>
													<td><button id="l" onclick="buscarLetra('l')"
															type="button">L</button></td>
												</tr>
												<tr>
													<td><button id="m" onclick="buscarLetra('m')"
															type="button">M</button></td>
													<td><button id="n" onclick="buscarLetra('n')"
															type="button">N</button></td>
													<td><button id="o" onclick="buscarLetra('o')"
															type="button">O</button></td>
												</tr>
												<tr>
													<td><button id="p" onclick="buscarLetra('p')"
															type="button">P</button></td>
													<td><button id="q" onclick="buscarLetra('q')"
															type="button">Q</button></td>
													<td><button id="r" onclick="buscarLetra('r')"
															type="button">R</button></td>
												</tr>
												<tr>
													<td><button id="s" onclick="buscarLetra('s')"
															type="button">S</button></td>
													<td><button id="t" onclick="buscarLetra('t')"
															type="button">T</button></td>
													<td><button id="u" onclick="buscarLetra('u')"
															type="button">U</button></td>
												</tr>
												<tr>
													<td><button id="v" onclick="buscarLetra('v')"
															type="button">V</button></td>
													<td><button id="w" onclick="buscarLetra('w')"
															type="button">W</button></td>
													<td><button id="x" onclick="buscarLetra('x')"
															type="button">X</button></td>
												</tr>
												<tr>
													<td><button id="y" onclick="buscarLetra('y')"
															type="button">Y</button></td>
													<td><button id="z" onclick="buscarLetra('z')"
															type="button">Z</button></td>
												</tr>
											</table>
										</table>
										<p><h5>
											Te quedan <input type="text" id="contador" name="contador"
												disabled value="6"> intentos
										</h5>
										</p>


									</div>
									<div class="8u">
										<div class="row half">
											<div class="12u" style="color: #000">
												<img id="horca" src="../images/ahorcado/horcaVacia.png"
													width="200" height="296" / alt="3">
											</div>
										</div>
										<div class="row half">
											<h3 id="palabra"></h3>
										</div>
									</div>
								</div>

							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Main -->

	</div>

	<!-- Footer -->
	<div id="footer" class="wrapper style2">
		<div class="container">
			<section>
				<header class="major">
					<h2>Arcade</h2>
					<span class="byline">Tributo a Tron y a los 80's</span> <img
						class="displayed" src="../images/flyns.png" alt="Flyn's Arcade" />
				</header>
				<form method="post" action="../principal.jsp">
					<div class="row half">
						<div class="12u">
							<ul class="actions">
								<li><input type="submit" value="Salir del juego"
									class="button alt" /></li>
							</ul>
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>
	<!-- /Footer -->
	<!-- Copyright -->
	<div id="copyright">
		<div class="container">
			<span class="copyright">Design: <a href="http://templated.co">TEMPLATED</a>
				Images: <a href="http://unsplash.com">Unsplash</a> (<a
				href="http://unsplash.com/cc0">CC0</a>)
			</span>
			<ul class="icons">
				<li><a href="#" class="fa fa-facebook"><span>Facebook</span></a></li>
				<li><a href="#" class="fa fa-twitter"><span>Twitter</span></a></li>
				<li><a href="#" class="fa fa-google-plus"><span>Google+</span></a></li>
			</ul>
		</div>
	</div>

	<script type="text/javascript">
		function buscarLetra(letra) {
			$("#" + letra).prop("disabled", true);

			if (!buscarRespuesta(letra)) {

				let intentos = parseInt($("#contador").val(), 10);
				if (intentos > 1) {
					$("#contador").val(intentos - 1);
					let url = "../images/ahorcado/horcaE0" + intentos;
					url += ".png";
					$("#horca").attr("src", url);
				} else {
					$("#contador").val(0);
					$("#horca").attr("src", "../images/ahorcado/horcaE01.png");
					$("#palabra").empty();
					$("#palabra").append(formatoPalabra());
					alert("Sorry!!! has perdido :(")
					desahabilitarBotones(true);
				}

			} else {
				$("#letrasEncontradas").val(
				$("#letrasEncontradas").val() + letra)
				verificarGanador();

			}
		}
		function verificarGanador() {
			let palabra = formatoPalabra();
			if ($("#palabra").text() == palabra) {
				$("#horca").attr("src", "../images/ahorcado/horcaWinner.png");
				alert("Felicidades!!! has ganado :)");
				desahabilitarBotones(true);

			}

		}
		function formatoPalabra() {
			let palabra = "";
			let palabraAdivinar = $("#respuesta").val();
			for (let i = 0; i < palabraAdivinar.length; i++) {
				palabra += palabraAdivinar[i].toUpperCase() + " "

			}
			return palabra;
		}
		function buscarRespuesta(letra) {

			let encontrado = false;
			let palabraAdivinar = $("#respuesta").val();
			let texto = "";

			for (let i = 0; i < palabraAdivinar.length; i++) {
				let carcterEncontrado = yaEncontrada(palabraAdivinar[i]);
				if (palabraAdivinar[i] == letra || carcterEncontrado) {
					texto += palabraAdivinar[i].toUpperCase() + " ";
					if (palabraAdivinar[i] == letra) {
						encontrado = true;
					}
				} else {
					texto += "_ ";
				}

			}
			$("#palabra").empty();
			$("#palabra").append(texto);
			return encontrado;
		}
		function yaEncontrada(letra) {
			let letras = $("#letrasEncontradas").val();
			for (let i = 0; i < letras.length; i++) {
				if (letras[i] == letra) {
					return true;
				}
			}

			return false;
		}
		function desahabilitarBotones(accion) {
			let teclas = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
					'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
					'w', 'x', 'y', 'z' ];
			for (let i = 0; i < teclas.length; i++) {
				$("#" + teclas[i]).prop("disabled", accion);
			}

		}
		$(document).ready(function() {
			desahabilitarBotones(true);
		});
		$("#iniciar").click(
				function(e) {
					
					let palabras = [ "perro", "gato", "perico", "elefante",
							"leon", "puma", "aguila", "zorro", ];
					let posPalabra = Math
							.floor(Math.random() * palabras.length);
					let palabraAdivinar = palabras[posPalabra]
					$("#respuesta").val(palabraAdivinar);
					let texto = "";
					for (let i = 0; i < palabraAdivinar.length; i++) {
						texto += "_ ";
					}
					$("#palabra").empty();
					$("#palabra").append(texto);
					desahabilitarBotones(false);

				});
	</script>

</body>


</html>