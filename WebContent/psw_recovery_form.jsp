<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>login - Phase Shift by TEMPLATED</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.dropotron.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/skel-layers.min.js"></script>
<script src="js/init.js"></script>
<noscript>
	<link rel="stylesheet" href="css/skel.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-wide.css" />
</noscript>
</head>
<body>

	<!-- Wrapper -->
	<div class="wrapper style1">

		<!-- Header -->
		<div id="header" class="skel-panels-fixed">
			<div id="logo">
				<h1>
					<a href="index.html"></a>Flyn's Arcade
				</h1>
				<span class="tag">Basado en Phase Shift by TEMPLATED</span>
			</div>
			<%@ include file="menu1.jsp"%>
			<!--Salir-->
		</div>
		<!-- Header -->


		<!-- Main -->
		<div id="main" class="displayed">
			<div class="container">
				<center>
					<img class="displayed" src="images/flyns.png" alt="Flyn's Arcade" />
				</center>
			</div>
		</div>
		<!-- /Main -->





		<!-- Footer -->
		<div id="footer" class="wrapper style2">
			<div class="container">
				<section>
					<header class="major">
						<h2>Recuperación de contraseña</h2>
					</header>



					<!-- 		<h1>Recuperación de contraseña</h1>   -->
					<p><%=(request.getAttribute("msg") != null) ? request.getAttribute("msg") : ""%></p>
					<form method="post" action="recoverPsw">
						<input class="text" type="text" name="login" id="login"
							placeholder="Usuario" />
						<button type="button" id="buscarPregunta">Buscar pregunta</button>
						<br>
						<h3 id="textoPregunta"></h3>
						<br> <input class="text formulario" type="text" name="respuesta"
							id="respuesta" placeholder="Respuesta a la pregunta"
							style="display: none" /> <br> <input class="text formulario"
							type="password" name="password" id="password"
							placeholder="Contraseña" style="display: none" /> <br> <input
							class="text formulario" type="password" name="passwordConfirm"
							id="passwordConfirm" placeholder="Confirme contraseña"
							style="display: none" /> <br> <input type="submit"
							value="Recuperar" style="display: none" class="button alt formulario" />
					</form>




				</section>
			</div>
		</div>
		<!-- /Footer -->

		<!-- Copyright -->
		<div id="copyright">
			<div class="container">
				<span class="copyright">Design: <a href="http://templated.co">TEMPLATED</a>
					Images: <a href="http://unsplash.com">Unsplash</a> (<a
					href="http://unsplash.com/cc0">CC0</a>)
				</span>
				<ul class="icons">
					<li><a href="#" class="fa fa-facebook"><span>Facebook</span></a></li>
					<li><a href="#" class="fa fa-twitter"><span>Twitter</span></a></li>
					<li><a href="#" class="fa fa-google-plus"><span>Google+</span></a></li>
				</ul>
			</div>
		</div>
		<script>
			$(document).ready(function() {
				$("#buscarPregunta").click(function(e) {
					
					let login = $("#login").val();
					$.ajax({
						type : "get",
						url : "buscarPregunta",
						data : "login=" + login,
						success : function(response) {
							//console.log(response);
							$("#textoPregunta").empty();
							$("#textoPregunta").append(response);
							$(".formulario").fadeIn();
						},
					});
				});
			});
		</script>
</body>
</html>