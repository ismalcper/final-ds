<%@ page import="java.util.Date"%>
<%@ include file="valida_sesion.jsp"%>


<!DOCTYPE HTML>
<!--
	Phase Shift by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->


<html>
<head>
<title>Inicio - Phase Shift by TEMPLATED</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.dropotron.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/skel-layers.min.js"></script>
<script src="js/init.js"></script>
<noscript>
	<link rel="stylesheet" href="css/skel.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-wide.css" />
</noscript>
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
</head>
<body>

	<!-- Wrapper -->
	<div class="wrapper style1">

		<!-- Header -->
		<div id="header" class="skel-panels-fixed">
			<div id="logo">
				<h1>
					<a href="principal.jsp">Flyn's Arcade</a>
				</h1>
				<span class="tag">Basado en Phase Shift by TEMPLATED</span>
			</div>
			<%@ include file="menu2.jsp"%>
			<!--AQUIIII-->
		</div>

		<!-- Banner -->
		<div id="banner" class="container">
			<section>
				<h2>Bienvenido al Arcade</h2>
				<h1>Selcciona un juego</h1>
				<!--<a href="#" class="button medium">Read More</a>-->
			</section>
		</div>

		<!-- Extra -->
		<div id="extra">
			<div class="container">
				<div class="row no-collapse-1">
					<section class="4u">
						<a href="#" class="image featured"><img
							src="images/torneoDelPoder/tenor.gif" alt="" width="444" height="333" ></a>
						<div class="box">
							<p>GATO</p>
							<a href="juegos/torneoDelPoder.jsp" class="button">Jugar</a>
						</div>
					</section>
					<section class="4u">
						<a href="#" class="image featured"><img
							src="images/ahorcado/tenor11.gif" alt="" width="444" height="333"></a>
						<div class="box">
							<p>Ahorcado.</p>
							<a href="juegos/ahorcado.jsp" class="button">Jugar</a>
						</div>
					</section>
					<section class="4u">
						<a href="#" class="image featured"><img
							src="images/howManyBurger/tenor111.gif" alt="" width="444" height="333"></a>
						<div class="box">
							<p>How many burgers.</p>
							<a href="juegos/howManyBurgers.jsp" class="button">Jugar</a>
						</div>
					</section>
				</div>
				<div class="row no-collapse-1">
					<section class="4u">
						<a href="#" class="image featured"><img
							src="images/fizzBazzBuzz/tenor1111.gif" alt="" width="444" height="333"></a>
						<div class="box">
							<p>Fizz Bazz Buzz.</p>
							<a href="juegos/fizzBazzBuzz.jsp" class="button">Jugar</a>
						</div>
					</section>
					
					<section class="4u">
						<a href="#" class="image featured"><img
							src="images/tossMachine/tenor11111.gif" alt="" width="444" height="333"></a>
						<div class="box">
							<p>Toss Machine.</p>
							<a href="juegos/tossMachine.jsp" class="button">Jugar</a>
						</div>
					</section>
					<section class="4u">
						<a href="#" class="image featured"><img
							src="images/trivia/tenor3.gif" alt="" width="444" height="333"></a>
						<div class="box">
							<p>Potro Trivia.</p>
							<a href="juegos/trivia.jsp" class="button">Jugar</a>
						</div>
					</section>

				</div>
			</div>
		</div>

		<!-- Main -->
		<div id="main">
			<div class="container">
				<div class="row">

					<!-- Content -->
					<div class="6u">
						<section>
							<ul class="style">
								<li class="fa fa-wrench">
									<h3>Reportar fallas</h3> <span>Hemos trabajado muy duro,
										pero si a pesar de nuestro esfuerzo encuentras una falla, <a
										href="mailto:david_antunezbarbosa@hotmail.com">reportala</a>.
								</span>
								</li>
								<li class="fa fa-leaf">
									<h3>Equipo de desarrollo</h3> <span> 
										<p>
											Clemente Cluadio Flores<br> 
											David Antunez Barbosa<br> 
											Fernanda Elizabeth Valdez Rocha<br> 
											Ismael Alcal� P�rez<br> 
											Yara Yescenia Rivas S�nchez<br> 
										</p>
								</span>
								</li>
							</ul>
						</section>
					</div>
					<div class="6u">
						<section>
							<ul class="style">
								<li class="fa fa-cogs">
									<h3>Arquitectura</h3> <span>Sistema en arquitectura
										Cliente/Servidor.<br>Servidor con arquitectura de capas.
								</span>
								</li>
								<li class="fa fa-road">
									<h3>Tecnolog&iacute;a</h3> <span>
										<p>
											JSP/Servlet<br> SQL<br> JS<br> html<br>
											css<br>
										</p>
								</span>
								</li>
							</ul>
						</section>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!-- Footer -->
	<div id="footer" class="wrapper style2">
		<div class="container">
			<section>
				<header class="major">
					<h2>Mensaje del día</h2>
					<span class="byline"><%=new Date()%></span>
				</header>
				<form method="post" action="#">
					<div class="row half">
						<div class="12u">
							<jsp:include page="mensaje.jsp" flush="true"></jsp:include>
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>

	<!-- Copyright -->
	<div id="copyright">
		<div class="container">
			<div class="copyright">
				<p>
					Design: <a href="http://templated.co">TEMPLATED</a> Images: <a
						href="http://unsplash.com">Unsplash</a> (<a
						href="http://unsplash.com/cc0">CC0</a>)
				</p>
				<ul class="icons">
					<li><a href="#" class="fa fa-facebook"><span>Facebook</span></a></li>
					<li><a href="#" class="fa fa-twitter"><span>Twitter</span></a></li>
					<li><a href="#" class="fa fa-google-plus"><span>Google+</span></a></li>
				</ul>
			</div>
		</div>
	</div>

</body>
</html>