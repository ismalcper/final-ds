<!DOCTYPE HTML>
<!--
	Phase Shift by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
<title>FlynsArcade</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.dropotron.min.js"></script>
<script src="js/skel.min.js"></script>
<script src="js/skel-layers.min.js"></script>
<script src="js/init.js"></script>
<noscript>
	<link rel="stylesheet" href="css/skel.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-wide.css" />
</noscript>
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
</head>
<body>

	<!-- Wrapper -->
	<div class="wrapper style1">

		<!-- Header -->
		<div id="header" class="skel-panels-fixed">
			<div id="logo">
				<h1>
					<a href="index.html"></a>Flyn's Arcade
				</h1>
				<span class="tag"></span>
			</div>
			<%@ include file="menu1.jsp"%>
			<!--AQUIIII OJOOOO-->
		</div>
		<!-- Header -->

		<!-- Page -->
		<div id="page" class="container">
			<section>
				<header class="major">
					<h2>FI - UAEM</h2>
					<span class="byline">El acceso est&aacute; restringido a
						usaurios registrados</span>
				</header>
			</section>
		</div>
		<!-- /Page -->

		<!-- Main -->
		<div id="main" class="displayed">
			<div class="container">
				<center>
					<img class="displayed" src="images/flyns.png" alt="Flyn's Arcade" />
				</center>
			</div>
		</div>
		<!-- /Main -->

	</div>

	<!-- Footer -->
	<div id="footer" class="wrapper style2">
		<div class="container">
			<section>
				<header class="major">
					<h2>Entrada</h2>
					<span class="byline">Credenciales de acceso</span>
				</header>
				<form method="post" action="ingresa">
					<!--url patterns-->
					<div class="row half">
						<div class="12u">
							<input class="text" type="text" name="name" id="name"
								placeholder="Name" />
						</div>
					</div>
					<div class="row half">
						<div class="12u">
							<input class="text" type="password" name="passwd" id="passwd"
								placeholder="Clave secreta" />
						</div>
					</div>
					<div class="row half">
						<div class="12u">
							<ul class="actions">
								<li><input type="submit" value="Ingresar"
									class="button alt" /></li>
							</ul>
						</div>
					</div>
				</form>
			</section>
		</div>
		<div class="row half">
			<div class="12u">
				<ul class="actions">
					<li><br></br> <br></br> 
						<a href="controlRegistro" class="button alt">Crear cuenta</a> 
						<a href="psw_recovery_form.jsp" class="button alt">Recuperar contraseņa</a>
						</li>
				</ul>
			</div>
		</div>




	</div>

	<!-- /Footer hshshsh -->

	<!-- Copyright -->
	<div id="copyright">
		<div class="container">
			<span class="copyright">Design: <a href="http://templated.co">TEMPLATED</a>
				Images: <a href="http://unsplash.com">Unsplash</a> (<a
				href="http://unsplash.com/cc0">CC0</a>)
			</span>
			<ul class="icons">
				<li><a href="#" class="fa fa-facebook"><span>Facebook</span></a></li>
				<li><a href="#" class="fa fa-twitter"><span>Twitter</span></a></li>
				<li><a href="#" class="fa fa-google-plus"><span>Google+</span></a></li>
			</ul>
		</div>
	</div>

</body>
</html>