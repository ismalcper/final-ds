<!DOCTYPE HTML>
<!--
	Phase Shift by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Top ten - Phase Shift by TEMPLATED</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-wide.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper -->
			<div class="wrapper style1">

				<!-- Header -->
					<div id="header" class="skel-panels-fixed">
						<div id="logo">
							<h1><a href="index.html">Flyn's Arcade</a></h1>
							<span class="tag">Basado en Phase Shift by TEMPLATED</span>
						</div>
						<%@ include file="menu2.jsp"%>
					</div>
				<!-- Header -->

				<!-- Page -->
					<div id="page" class="container">
						<div class="row">
		
							<!-- Sidebar -->
							<div id="sidebar" class="4u">
								<section>
									<header class="major">
										<h2>Juegos</h2>
									</header>
									<div class="row half">
										<section class="6u">
											<ul class="default">
												<li><a href="#">Ahorcado</a></li>
												<li><a href="#">Fizz Bazz Buzz</a></li>
												<li><a href="#">Anagrama</a></li>

											</ul>
										</section>
										<section class="6u">
											<ul class="default small">
												<li><a href="#">Toss Machine</a></li>
												<li><a href="#">How many burgers</a></li>
												<li><a href="#">Potro trivia</a></li>
											</ul>
										</section>
									</div>
								</section>
								<section>
									<header class="major">
										<h2>Líderes</h2>
									</header>
									<ul class="default">
										<li><b>Ahorcado: </b> Jorge Buendia</li>
										<li><b>Fizz Bazz Buzz: </b> Daniel Ramirez Flores</li>
										<li><b>Anagrama: </b>Eduardo M&aacute;rquez Madrid</li>
										<li><b>Toss Machine: </b>Jes&uacute;s Cruz</li>
										<li><b>How many Burgers: </b>Miguel Angel Ch&aacute;vez</li>
										<li><b>Potro trivia: </b>Abraham Pardo</li>
									</ul>
								</section>
							</div>
							
							<!-- Content -->
							<div id="content" class="8u skel-cell-important">
								<section>
									<header class="major">
										<h2>Records</h2>
										<span class="byline">xico</span>
									</header>
									<center><img src="images/hs.png" alt="trofeo"/></center>
									<p> 
										<ol>
											<li><b>Ahorcado:</b> 30</li>
											<li><b>Fizz Bazz Buzz:</b> 0</li>
											<li><b>Anagrama:</b>100</li>
											<li><b>Toss machine:</b> 5</li>
											<li><b>How many burgers:</b> 3</li>
											<li><b>Potro trivia:</b> 500</li>
										</ol>										
									</p>
								</section>
							</div>
		
						</div>
					</div>
				<!-- /Page -->

				<!-- Main -->
					<div id="main">
						<div class="container">
							<div class="row"> 
								
								<!-- Content -->
								<div class="6u">
									<section>
										<ul class="style">
											<li class="fa fa-wrench">
												<h3>Reportar fallas</h3>
												<span>Hemos trabajado muy duro, pero si a pesar de nuestro esfuerzo encuentras una falla, <a href="mailto:fchavez19@gmail.com">reportala</a>.</span> </li>
											<li class="fa fa-leaf">
												<h3>Equipo de desarrollo</h3>
												<span>
													<u>Equipo 1</u>
													<p>
													Mar&iacute;a Dolores Duran<br>
													Jos&eacute; Caballero<br>
													Balam Aguilar<br>
													Marcelo Romero 
													</p>												
												</span> </li>
										</ul>
									</section>
								</div>
								<div class="6u">
									<section>
										<ul class="style">
											<li class="fa fa-cogs">
												<h3>Arquitectura</h3>
												<span>Sistema en arquitectura Cliente/Servidor.<br>Servidor con arquitectura de capas.</span> </li>
											<li class="fa fa-road">
												<h3>Tecnolog&iacute;a</h3>
												<span>
													<p>													
													JSP/Servlet<br>
													SQL<br>
													JS<br>
													html<br>
													css<br>
													</p>
												</span> </li>
										</ul>
									</section>
								</div>
							</div>
						</div>
					</div>
				<!-- /Main --> 

	</div>

	<!-- Footer -->
		<div id="footer" class="wrapper style2">
			<div class="container">
				<section>
					<header class="major">
						<h2>Arcade</h2>
						<span class="byline">Tributo a Tron y a los 80's</span>
						<img class="displayed" src="images/flyns.png" alt="Flyn's Arcade"/>
					</header>
					<form method="post" action="principal.html">						
						<div class="row half">
							<div class="12u">
								<ul class="actions">
									<!--<li>
										<input type="submit" value="Salir del juego" class="button alt" />
									</li>-->
								</ul>
							</div>
						</div>
					</form>
				</section>
			</div>
		</div>
	<!-- /Footer -->

	<!-- Copyright -->
		<div id="copyright">
			<div class="container"> <span class="copyright">Design: <a href="http://templated.co">TEMPLATED</a> Images: <a href="http://unsplash.com">Unsplash</a> (<a href="http://unsplash.com/cc0">CC0</a>)</span>
				<ul class="icons">
					<li><a href="#" class="fa fa-facebook"><span>Facebook</span></a></li>
					<li><a href="#" class="fa fa-twitter"><span>Twitter</span></a></li>
					<li><a href="#" class="fa fa-google-plus"><span>Google+</span></a></li>
				</ul>
			</div>
		</div>

	</body>
</html>