package mx.uaemex.fi.diseno.flyns.control;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import mx.uaemex.fi.diseno.nrda.model.PreguntasDAO;
import mx.uaemex.fi.diseno.nrda.model.PreguntasDAODerbyImp;
import mx.uaemex.fi.diseno.nrda.model.UsuarioDAO;
import mx.uaemex.fi.diseno.nrda.model.UsuarioDAODerbyImp;
import mx.uaemex.fi.diseno.nrda.model.data.Pregunta;
import mx.uaemex.fi.diseno.nrda.model.data.Usuario;

/**
 * Servlet implementation class ControlRegistro
 * CONSULTAR� TODAS LAS PREGUNTAS DE LA BASE DE DATOS PARA SER MOSTRADAS EN EL FORMULARIO DE REGISTRO

 * @author: Lorena G�mez, Monica Salinas, Diana Quintana, Ismael Alcal�, Edson 

 * @version: 16/12/2020

 */
 
@WebServlet("/controlRegistro")
public class ControlRegistro extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PreguntasDAO dao;
	private DataSource ds;
       
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			InitialContext cxt = new InitialContext();
			ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/nrda");  ///////ds
			if (ds == null) {
				throw new ServletException("NO encontr� la fuente de datos");
			}
		} catch (NamingException e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Entrando a Control");
		
		PreguntasDAODerbyImp daoReal;
		ArrayList<Pregunta> preguntas = new ArrayList<>();  //Obtenemos todas las preguntas
		
		try {
			Connection con = ds.getConnection();
			daoReal = new PreguntasDAODerbyImp();
			daoReal.setConexion(con);
			this.dao = daoReal;			
			preguntas = this.dao.get(); //Lee de BD
			
			System.out.println("* * * * * LISTA DE PREGUNTAS DESDE SERVLET * * * * * ");
			Iterator<Pregunta> it = preguntas.iterator();
			while(it.hasNext()){
				Pregunta item=it.next();
			    System.out.println(item.getId() + " - "+ item.getPregunta());
			}
			System.out.println("* * * * * TERMINA LISTA DE PREGUNTAS * * * * * ");
			
			//String mensajito = "HOoOoOolAaAaAaAaA";
			request.setAttribute("preguntas", preguntas);
			RequestDispatcher rd = request.getRequestDispatcher("jugador/registro.jsp");
			rd.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
