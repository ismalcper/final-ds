package mx.uaemex.fi.diseno.flyns.control;



import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



/**
 * Servlet implementation class ControlHowManyBurgers

 * @author: Yara Rivas, David Antunez, Clemente Claudio, Fernanda Valdez, Ismael Alcala

 * @version: 16/12/2020
 */
@WebServlet("/controlHMB")
public class ControlHowManyBurgers extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Random rand;
	private int numRand; 
	private int intentos;

	

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.rand = new Random();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
			
		HttpSession ses;
		ses = request.getSession();   //A) Si la sesion ya existe entonces el cliente ya tiene algo
									//B) Que la sesi�n no exista, aparta un espacio de momoria, asigna un identificador
		
		Integer hComidas = (Integer)ses.getAttribute("hComidas");
			
		
		//PRIMERA VEZ hComidas == null
		if (hComidas == null) {
			hComidas = this.rand.nextInt(100)+1;
			ses.setAttribute("hComidas", hComidas);			
			ses.setAttribute("Intentos", 0);
		}
				
		//SI YA EXISTE, YA SE HA JUGADO
		int valor = Integer.parseInt(request.getParameter("numero"));
	
		intentos = (Integer)(ses.getAttribute("Intentos"));
		ses.setAttribute("Intentos", intentos+1);
		
		if (valor == hComidas) {
			ses.setAttribute("intentos", intentos+1);
			ses.setAttribute("mensaje", "Ganaste");  //solo para identificar
			RequestDispatcher rd = request.getRequestDispatcher("juegos/ganadorHMB.jsp");
			rd.forward(request, response);	
		/////	salida.println("<h2>"+ "!!Acertaste en "+ String.valueOf((Integer)(ses.getAttribute("Intentos")) ) + " Intentos!!" +"<h2>");
			
			hComidas = this.rand.nextInt(100)+1;
			ses.setAttribute("hComidas", hComidas);
			intentos = 0;
			ses.setAttribute("Intentos", intentos);
		}else if ( valor > hComidas ) {
			ses.setAttribute("intentos", intentos+1);
			ses.setAttribute("mensaje", "Menos");  //solo para identificar
			RequestDispatcher rd = request.getRequestDispatcher("juegos/ganadorHMB.jsp");
			rd.forward(request, response);
		}else {
			ses.setAttribute("intentos", intentos+1);
			ses.setAttribute("mensaje", "Mas");  //solo para identificar
			RequestDispatcher rd = request.getRequestDispatcher("juegos/ganadorHMB.jsp");
			rd.forward(request, response);
		}
			
	
	}


	
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
