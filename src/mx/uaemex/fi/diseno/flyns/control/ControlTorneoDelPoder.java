package mx.uaemex.fi.diseno.flyns.control;

import java.io.IOException;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ControlTorneoDelPoder
 * 
 * Generar n�meros Aleatorios y comparar PC vs Gamer

 * @author: Yara Rivas, David Antunez, Clemente Claudio, Fernanda Valdez, Ismael Alcala

 * @version: 16/12/2020

 */
@WebServlet("/torneoPoder")
public class ControlTorneoDelPoder extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Random rand;
	// private int randPC;
	// private int randUser;
	private Cookie cookie;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		this.rand = new Random();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession ses;
		ses = request.getSession();

		Integer powerPC = (Integer) ses.getAttribute("powerPC");
		Integer powerUser = (Integer) ses.getAttribute("powerUser");

		//Primera vez que Juega el usuario
		if (powerPC == null && powerUser == null) {
			powerPC = this.rand.nextInt(10000) + 1;
			powerUser = this.rand.nextInt(10000) + 1;
			ses.setAttribute("powerPC", powerPC);
			ses.setAttribute("powerUser", powerUser);
		}
		
		//Si hay variables de session ya se ha jugado 
		
		//Comparamos los valores del PC vs el Usuario
		if (powerPC > powerUser) {  //Gana PC
			powerPC = this.rand.nextInt(10000) + 1;
			powerUser = this.rand.nextInt(10000) + 1;
			ses.setAttribute("powerPC", powerPC);
			ses.setAttribute("powerUser", powerUser);
			
			//Enviamos los datos a pantalla para desplegar resultados
			request.setAttribute("powerPC", powerPC);
			request.setAttribute("powerUser", powerUser);
			request.setAttribute("ganador", "PC");
			RequestDispatcher rd = request.getRequestDispatcher("juegos/ganadorTorneoPoder.jsp");
			rd.forward(request, response);	
		}else {
			if (powerPC < powerUser) {  //Gana Usuario
				powerPC = this.rand.nextInt(10000) + 1;
				powerUser = this.rand.nextInt(10000) + 1;
				ses.setAttribute("powerPC", powerPC);
				ses.setAttribute("powerUser", powerUser);
				
				//Enviamos los datos a pantalla para desplegar resultados
				request.setAttribute("powerPC", powerPC);
				request.setAttribute("powerUser", powerUser);
				request.setAttribute("ganador", "USUARIO");
				RequestDispatcher rd = request.getRequestDispatcher("juegos/ganadorTorneoPoder.jsp");
				rd.forward(request, response);

				//EN ESTE CASO SE GANA Y SE ALMACENAR� EL powerUser como record en la base de datos
				
			}else {
				if (powerPC == powerUser) {  //Empate
					powerPC = this.rand.nextInt(10000) + 1;
					powerUser = this.rand.nextInt(10000) + 1;
					ses.setAttribute("powerPC", powerPC);
					ses.setAttribute("powerUser", powerUser);
					
					//Enviamos los datos a pantalla para desplegar resultados
					request.setAttribute("powerPC", powerPC);
					request.setAttribute("powerUser", powerUser);
					request.setAttribute("ganador", "EMPATE");
					RequestDispatcher rd = request.getRequestDispatcher("juegos/ganadorTorneoPoder.jsp");
					rd.forward(request, response);	
				}
			}
				
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
