package mx.uaemex.fi.diseno.flyns.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ControlXBotellas

 * @author: Yara Rivas, David Antunez, Clemente Claudio, Fernanda Valdez, Ismael Alcala

 * @version: 16/12/2020

 */
@WebServlet("/controlBotellas")
public class ControlXBotellas extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession ses;
		ses = request.getSession();

		Integer xBotellas = (Integer) ses.getAttribute("xBotellas");
		String recibo = request.getParameter("xBotellas");
		Integer numeroBotellas;
		
		if (recibo == "" || recibo.length() == 0) {
			numeroBotellas = -1;
		}else {
			numeroBotellas = Integer.parseInt(recibo); //Recuperamos lo que ingres� el usuario en INTEGER
		}
				
		// Primera vez que Juega el usuario
		if (xBotellas == null ) {
			ses.setAttribute("xBotellas", numeroBotellas);
		}

		if ((numeroBotellas >= 0) && (numeroBotellas < 101)) { // Est� dentro del rango 0-100
			request.setAttribute("xBotellas", numeroBotellas);
			request.setAttribute("validacion", true);
			RequestDispatcher rd = request.getRequestDispatcher("juegos/cantarXBotellas.jsp");
			rd.forward(request, response);
		} else {
			request.setAttribute("validacion", false);
			RequestDispatcher rd = request.getRequestDispatcher("juegos/cantarXBotellas.jsp");
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
