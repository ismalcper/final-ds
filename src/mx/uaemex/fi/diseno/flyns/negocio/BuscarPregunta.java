package mx.uaemex.fi.diseno.flyns.negocio;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import mx.uaemex.fi.diseno.nrda.model.PreguntasDAO;
import mx.uaemex.fi.diseno.nrda.model.PreguntasDAODerbyImp;
import mx.uaemex.fi.diseno.nrda.model.UsuarioDAO;
import mx.uaemex.fi.diseno.nrda.model.UsuarioDAODerbyImp;
import mx.uaemex.fi.diseno.nrda.model.data.Pregunta;
import mx.uaemex.fi.diseno.nrda.model.data.Usuario;





/**
 * Servlet implementation class Cadenero
 */
@WebServlet("/buscarPregunta")
public class BuscarPregunta extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UsuarioDAO dao;
	private PreguntasDAO daop;
	private DataSource ds;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		try {
			//
			InitialContext cxt = new InitialContext();
			ds=(DataSource) cxt.lookup("java:/comp/env/jdbc/nrda");
			if (ds == null) {
				throw new ServletException("No encontro la fuente de los datos");
			}
		} catch (NamingException e) {
			// TODO: handle exception
			throw new ServletException(e);
		}
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UsuarioDAODerbyImp daoReal;
		Usuario u,uLeido;
		//Leer los datos del formulario
		String login =request.getParameter("login"); //---
		u = new Usuario(); //
		u.setLogin(login);
		
		try {
			
			Connection  con = ds.getConnection();
			daoReal = new UsuarioDAODerbyImp();
			daoReal.setConexion(con);
			this.dao = daoReal;
			uLeido = this.dao.get(u); //leer de la base de datos
			
		    if(login.compareTo(uLeido.getLogin())==0) {
		    	PreguntasDAODerbyImp daoRealPre;
		    	Pregunta pLeida;
		    	Pregunta pregunta = new Pregunta();
		    	pregunta.setId(uLeido.getPregunta().getId());
		    	daoRealPre = new PreguntasDAODerbyImp();
		    	daoRealPre.setConexion(con);
				this.daop = daoRealPre;
				pLeida = this.daop.get(pregunta);
				
				String txtPregunta = "�"+pLeida.getPregunta().toUpperCase()+"?";
				String idUsuario = uLeido.getLogin();
		    	
		    	
			 	
		    	response.setContentType("text/plain"); 
		        response.setCharacterEncoding("UTF-8");
		    	response.getWriter().write(txtPregunta);
		 
		    	//response.sendRedirect("principal.jsp");
		    }else {
		    	response.sendRedirect("recuperaLogin.jsp");
		    }
		}catch(SQLException e) {
			
			e.printStackTrace();
		}
		
		//3-Comparar informaci�n vs lo que sali� del DAO
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		doGet(request, response);
	}

}
