package mx.uaemex.fi.diseno.flyns.negocio;

import java.io.IOException;
import java.sql.Connection;

import javax.naming.InitialContext;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import mx.uaemex.fi.diseno.commons.NombreLatino;
import mx.uaemex.fi.diseno.nrda.model.UsuarioDAO;
import mx.uaemex.fi.diseno.nrda.model.UsuarioDAODerbyImp;
import mx.uaemex.fi.diseno.nrda.model.data.Pregunta;
import mx.uaemex.fi.diseno.nrda.model.data.TipoUsuario;
import mx.uaemex.fi.diseno.nrda.model.data.Usuario;

/**
 * Servlet implementation class Registro

 * @author: Yara Rivas, David Antunez, Clemente Claudio, Fernanda Valdez, Ismael Alcala

 * @version: 16/12/2020

 */
@WebServlet(description = "Registro de Usuario", urlPatterns = { "/registrar" })
public class Registro extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UsuarioDAO dao;
	protected DataSource ds;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Registro() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		try {
			InitialContext cxt = new InitialContext();
			ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/nrda");
			if (ds == null) {
				throw new ServletException("No encuentra la fuente de datos");
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new ServletException();
		}

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UsuarioDAODerbyImp daoReal;
		Usuario u, uLeido;

		// Leer los datos del formulario
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String primer_apellido = request.getParameter("primer_apellido");
		String segundo_apellido = request.getParameter("segundo_apellido");
		String nombre = request.getParameter("nombre");
		Integer edad = Integer.parseInt(request.getParameter("edad"));
		Integer tipo = 1; // 1 = JUGADOR
		Integer idPregunta = Integer.parseInt(request.getParameter("idPregunta"));
		String respuesta = request.getParameter("respuesta");
		
		System.out.println("DATOS QUE LLEGAN");
		System.out.println(login);
		System.out.println(password);
		System.out.println(password2);
		System.out.println(primer_apellido);
		System.out.println(segundo_apellido);
		System.out.println(nombre);
		System.out.println(edad);
		System.out.println(tipo);
		System.out.println(idPregunta);
		System.out.println(respuesta);
		
		//Creamos instancia de clase Usuario
		u = new Usuario();
		u.setLogin(login);
		u.setPassword(password);
		u.setNombre(new NombreLatino(nombre, primer_apellido, segundo_apellido));
		u.setEdad(edad);
		u.setTipo(new TipoUsuario(1));
		u.setPregunta(new Pregunta(idPregunta));
		u.setRespuesta(respuesta);
	
		//Registrar usuario mediante el DAO
		try {
			Connection con = ds.getConnection();
			daoReal = new UsuarioDAODerbyImp();
			daoReal.setConexion(con);
			
			this.dao = daoReal;
			
			//this.dao.crear(u);
			if (this.dao.crear(u)) {
				response.sendRedirect("index.jsp");
			//	System.out.println("alert('Registro Exitoso');");
				System.out.println("Registro Exitoso");
			}else {
				System.out.println("alert('Algo Fall�');");
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
