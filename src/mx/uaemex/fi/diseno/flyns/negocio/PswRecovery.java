package mx.uaemex.fi.diseno.flyns.negocio;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import mx.uaemex.fi.diseno.nrda.error.ConexionException;
import mx.uaemex.fi.diseno.nrda.error.EntidadNoValidaException;
import mx.uaemex.fi.diseno.nrda.error.RegistroNotFoundException;
import mx.uaemex.fi.diseno.nrda.model.UsuarioDAO;
import mx.uaemex.fi.diseno.nrda.model.UsuarioDAODerbyImp;
import mx.uaemex.fi.diseno.nrda.model.data.Pregunta;
import mx.uaemex.fi.diseno.nrda.model.data.Usuario;

/**
 * Servlet implementation class PswRecovery

 * @author: Yara Rivas, David Antunez, Clemente Claudio, Fernanda Valdez, Ismael Alcala

 * @version: 16/12/2020
 */
@WebServlet(description = "Servlet para manejar la recuperaci�n de contrase�a", urlPatterns = { "/recoverPsw" })
public class PswRecovery extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final static Logger LOGGER = Logger.getLogger(PswRecovery.class.getName());
	private UsuarioDAO dao;
    private DataSource ds;
    HttpSession ses;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			InitialContext cxt = new InitialContext();
			ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/nrda"); //ds
			if (ds == null) {
				throw new ServletException("No encuentra la fuente de datos! :(");
			}
		} catch (NamingException e) {
			throw new ServletException(e);
			
		}
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//crear una especie de mensaje para usar FLASH
		String msg = null;
		//obtener todos los campos del formulario (login, pregunta, contrase�a nueva, confirmaci�n de contrase�a nueva)
		String login = request.getParameter("login");
		String respuesta = request.getParameter("respuesta");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		//verificar que vengan todos y si no viene alguno, devolver un 400 Bad Request
		boolean requestIsCorrect = (login != null) && (respuesta != null) 
				&& (password != null) && (passwordConfirm != null);
		if (requestIsCorrect) {
			//trabajar normalmente la solicitud
			Usuario u = new Usuario(login);
			u.setRespuesta(respuesta);
			try {
				Connection con = ds.getConnection();
				UsuarioDAODerbyImp daoReal = new UsuarioDAODerbyImp(); 
				daoReal.setConexion(con);
				dao = daoReal;
				Usuario uLeido = dao.get(u);
				if (uLeido != null) {
					//pon la contrase�a nueva
					if (password.equals(passwordConfirm)) {
						uLeido.setPassword(password);
						dao.update(uLeido);
						//indicar al usuario que ya puede iniciar sesi�n (redireccionar)
						//msg = "Se cambi� con �xito su contrase�a, ya puede iniciar sesi�n";
						LOGGER.log(Level.INFO,"Actualizada contrase�a");
					} else {
						msg = "El usuario ingresado no existe, la respuesta "
								+ "es incorrecta o las contrase�as no coinciden";
					}
					
				} else {
					//TODO algo sali� mal (el usuario no existe FLASH)
					msg = "El usuario ingresado no existe, la respuesta "
							+ "es incorrecta o las contrase�as no coinciden";
				}
				ses = request.getSession();
				ses.setAttribute("msg",msg);
				if (msg == null) {
					response.sendRedirect("index.jsp");
				} else {
					response.sendRedirect("psw_recovery_form.jsp");
				}
				
				//Servlet JSP communication
				/*RequestDispatcher reqDispatcher = getServletConfig().getServletContext()
						.getRequestDispatcher("/psw_recovery_form.jsp");
				reqDispatcher.forward(request,response);*/
			} catch (ConexionException e) {
				//algo sali� mal
				//FLASH (intente m�s tarde)
				msg = "No se pudo completar la operaci�n, intente m�s tarde";
				ses = request.getSession();
				ses.setAttribute("msg",msg);
				response.sendRedirect("psw_recovery_form.jsp");
			} catch (RegistroNotFoundException  e) {
				//algo sali� mal
				//algo sali� mal (el usuario no existe FLASH)
				msg = "El usuario ingresado no existe, o la respuesta es incorrecta";
				ses = request.getSession();
				ses.setAttribute("msg",msg);
				response.sendRedirect("psw_recovery_form.jsp");
			}catch ( EntidadNoValidaException e) {
				//algo sali� mal
				//FLASH (intente m�s tarde)
				msg = "No se pudo completar la operaci�n, intente m�s tarde";
				LOGGER.log(Level.SEVERE,"Bug en el servlet"+e.getMessage());
				ses = request.getSession();
				ses.setAttribute("msg",msg);
				response.sendRedirect("psw_recovery_form.jsp");
			} catch (SQLException e) {
				//algo sali� mal
				//FLASH (intente m�s tarde)
				msg = "No se pudo completar la operaci�n, intente m�s tarde";
				LOGGER.log(Level.SEVERE,"Bug en el servlet"+e.getMessage());
				ses = request.getSession();
				ses.setAttribute("msg",msg);
				response.sendRedirect("psw_recovery_form.jsp");
			}
			
			/*RequestDispatcher reqDispatcher = getServletConfig().getServletContext()
					.getRequestDispatcher("psw_recovery_form.jsp");
			reqDispatcher.forward(request,response);*/
		} else {
			//lanzar bad request
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
