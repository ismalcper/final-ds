package mx.uaemex.fi.diseno.flyns.negocio;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import mx.uaemex.fi.diseno.nrda.model.UsuarioDAO;
import mx.uaemex.fi.diseno.nrda.model.UsuarioDAODerbyImp;
import mx.uaemex.fi.diseno.nrda.model.data.Usuario;

/**
 * Servlet implementation class Cadenero
 * 
 * @author: Yara Rivas, David Antunez, Clemente Claudio, Fernanda Valdez, Ismael Alcala

 * @version: 16/12/2020
 */
@WebServlet(description = "Servlet que revisa las credenciales de ingreso", urlPatterns = { "/ingresa" })
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UsuarioDAO dao;
	private DataSource ds;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			InitialContext cxt = new InitialContext();
			ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/nrda");  ///////ds
			if (ds == null) {
				throw new ServletException("NO encontr� la fuente de datos");
			}
		} catch (NamingException e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UsuarioDAODerbyImp daoReal;
		Usuario u, uLeido;
		//leer los datos del formulario
		String login = request.getParameter("name");
		String passwd = request.getParameter("passwd");
		u = new Usuario();
		u.setLogin(login);
		//Hacer una consulta mediante el DAO
		try {
			Connection con = ds.getConnection();
			daoReal = new UsuarioDAODerbyImp();
			daoReal.setConexion(con);
			this.dao = daoReal;			
			uLeido = this.dao.get(u); //Lee de BD
		//	response.getWriter().append(passwd);
		//	response.getWriter().append(login);
			//Comparar la info (formulario vs DAO)
			if(passwd.compareTo(uLeido.getPassword())==0) {
				//ponerlo en la sesion sig 2 lineas
				HttpSession ses = request.getSession();
				ses.setAttribute("usuario", uLeido);
				response.sendRedirect("principal.jsp");
			}else {
				response.sendRedirect("index.jsp");
			}   
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
