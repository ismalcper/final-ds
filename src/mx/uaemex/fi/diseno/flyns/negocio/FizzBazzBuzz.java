package mx.uaemex.fi.diseno.flyns.negocio;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/FizzBazzBuzz")
public class FizzBazzBuzz extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String cadena=request.getParameter("numero");
		String texto="";

		 int numero = Integer.parseInt(cadena);

		 for (int i=1; i<=numero; i++) {

		 if((i % 3)==0) {
			 if((i % 5)==0) {
				 texto+=i+" Buzz"+"\n";
			 }else {
				  texto+=i+" Fizz"+"\n";
			 }
			
		 //response.getWriter().append(i+" Fizz"+"\n");

		 }else if((i % 5)==0) {
			 if((i % 3)==0) {
				 texto+=i+" Buzz"+"\n";
			 }else {
				 texto+=i+" Bazz"+"\n";
			 }
			 
		 //response.getWriter().append(i+"bazz "+"\n");

		 }else {
			 texto+=i+"\n";
		 //response.getWriter().append(i+"\n");

		 }
		
		
		
	}
		 response.getWriter().write(texto);
}
}